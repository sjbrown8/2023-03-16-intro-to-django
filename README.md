# intro-to-django

## Setup Instructions

1. Create the virtual environment:

    ```bash
    python3 -m venv env
    ```

2. Activate the virtual environment:

    ```bash
    source env/bin/activate
    ```

3. Install the requirements:

    ```bash
    pip install -r requirements.txt
    ```

4. Migrate the database:

    ```bash
    python manage.py migrate
    ```

5. Load the fixture data:

    ```bash
    python manage.py loaddata polls/fixtures/questions.yaml
    ```

6. Create a superuser:

    ```bash
    python manage.py createsuperuser
    ```

7. Run the tests:

    ```bash
    python manage.py test
    ```

8. Run the development server:

    ```bash
    python manage.py runserver
    ```

9. Open the admin page:

    ```bash
    open http://localhost:8000/admin
    ```

    - Add a few questions and choices.

10. Open the polls page:

    ```bash
    open http://localhost:8000/polls
    ```
