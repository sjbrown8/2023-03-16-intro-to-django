"""
Views for polls app.
"""

from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.utils import timezone

from . import models


def index(request: HttpRequest) -> HttpResponse:
    """
    Index view for polls app.
    """
    question_list = models.Question.objects.all()
    context = {'question_list': question_list}
    return render(request, 'polls/index.html', context)


def detail(request: HttpRequest, question_id: int) -> HttpResponse:
    """
    Detail view for polls app.

    :param request: HttpRequest
    :param question_id: The id of the question.
    """
    question = get_object_or_404(models.Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question': question})


def results(request: HttpRequest, question_id: int) -> HttpResponse:
    """
    Results view for polls app.

    :param request: HttpRequest
    :param question_id: The id of the question.
    """
    question = get_object_or_404(models.Question, pk=question_id)
    return render(request, 'polls/results.html', {'question': question})


def vote(request: HttpRequest, question_id: int) -> HttpResponse:
    """
    Vote view for polls app.

    :param request: HttpRequest
    :param question_id: The id of the question to vote on.
    """
    question = get_object_or_404(models.Question, pk=question_id)

    selected_choice = question.choice_set.get(pk=request.POST['choice'])
    selected_choice.votes += 1
    selected_choice.save()
    return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
