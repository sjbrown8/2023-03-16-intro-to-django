"""
Polls admin.
"""

from django.contrib import admin

from .models import Choice, Question


class ChoiceInline(admin.StackedInline):
    """
    Choice inline for Question admin.
    """
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    """
    Question admin.
    """
    fieldsets = [
        (None,               {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]

admin.site.register(Question, QuestionAdmin)
