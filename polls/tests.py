"""
Tests for the polls app.
"""

import datetime

import django.http
import django.test
import django.utils.timezone

from .models import Question
import polls.models 

NOW = django.utils.timezone.now()
YESTERDAY = NOW - datetime.timedelta(days=1)


def create_question(
    question_text: str,
    publication_date: datetime.datetime = YESTERDAY,
    choices: list[str] = None,
) -> polls.models.Question:
    """
    Create a question for testing purposes.
    
    Create a question with the given `question_text` and published the given number of days offset
    `offset_in_days` to now (negative for questions published in the past, positive for questions
    that have yet to be published).

    If `choices` is given, create those choices for the question.

    :param question_text: The text of the question.
    :param publication_date: The date the question was published. Defaults to yesterday.
    :param choices: A list of choices for the question.
    
    :return: The created question.
    """
    question = polls.models.Question.objects.create(
        question_text=question_text,
        pub_date=publication_date,
    )
    for choice in choices or []:
        question.choice_set.create(choice_text=choice)
    return question


class TestPollsview(django.test.TestCase):
    
    def test_index(self) -> None:
        """
        If no questions exist, an appropriate message is displayed.
        """
        response = self.client.get('/polls/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No polls are available.")
        self.assertQuerysetEqual(response.context['question_list'], [])

    def test_index_with_a_question(self) -> None:
        """
        Assert that a question is displayed on the index page.        
        """
        question_text = "Past question."
        create_question(question_text=question_text)
        response = self.client.get('/polls/')
        self.assertEqual(response.status_code, django.http.HttpResponse.status_code)
        self.assertContains(response, question_text)

    def test_index_contains_only_five_most_recent_questions(self) -> None:
        """
        Assert that the index page only contains the five most recent questions.
        """
        question_text = "Past question."
        for i in range(1, 7):
            create_question(
                question_text=f"{question_text} {i}",
                publication_date=NOW - datetime.timedelta(days=i),
            )
        response = self.client.get('/polls/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, question_text)
        self.assertNotContains(response, "Past question. 6")

class TestQuestionDetailView(django.test.TestCase):

    def test_get_question(self) -> None:
        """
        The detail view of a question with a pub_date in the past displays the question's text.
        """
        past_question = create_question(question_text="Past question.")
        url = f'/polls/{past_question.id}/'
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, past_question.question_text)


class TestVoteView(django.test.TestCase):

    def test_vote(self) -> None:
        """
        Assert that voting on a question works.
        """
        question = create_question(
            question_text="Past question.",
            choices=["Choice 1", "Choice 2"],
        )
        self.client.post(f'/polls/{question.id}/vote/', data={'choice': 1})
        result_response = self.client.get(f"/polls/{question.id}/results/")
        self.assertEqual(result_response.status_code, 200)
        self.assertContains(result_response, "Choice 1 -- 1 vote")

    def test_vote_with_no_choice(self) -> None:
        """
        Assert that voting on a question without a choice returns an error.
        """
        question = create_question(
            question_text="Past question.",
            choices=["Choice 1", "Choice 2"],
        )
        response = self.client.post(f'/polls/{question.id}/vote/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "You did not select a choice.")
